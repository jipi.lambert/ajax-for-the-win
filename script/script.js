const xhr = new XMLHttpRequest();
const app = document.getElementById("app");
const form_data = document.getElementById("new_note");

let getNotes = () => {
    xhr.open("GET", 'http://localhost:3000/api/v1/notes');
    xhr.onload = () => {
        if (xhr.status == 200) {
            processNotes(JSON.parse(xhr.response), attachListener);
        } else {
            console.log("failed request" + xhr.status)
        }
    }
    xhr.send();
}

let processNotes = (notes, cb) => {
    for (let i = 0; i < notes.length; i++){
        let el_wrapper = document.createElement('div');
        let h2 = document.createElement("h2");
        let p = document.createElement('p');
        let del = document.createElement('p');

        del.append("Delete note :" + notes[i].title);
        del.classList.add('delete');
        del.dataset.noteId = notes[i]._id;
        el_wrapper.classList.add("note");
        h2.append(notes[i].title);
        p.append(h2);
        p.append(notes[i].note); 
        el_wrapper.append(p);
        el_wrapper.append(del);

        app.append(el_wrapper);
    }
    cb();
}

let attachListener = () => {
    const remove = document.querySelectorAll(".delete");
    for (let i = 0; i < remove.length; i++) {
        remove[i].addEventListener("click", (e) => {
            let note_id = e.target.dataset.noteId;
            deleteNote(note_id);
        });
    }
}

let deleteNote = (note_id) => {
    xhr.open("DELETE", 'http://localhost:3000/api/v1/notes/' + note_id);
    xhr.onload = () => {
        if (xhr.status == 200) {
            console.log(xhr.response);
            deleteNotes(getNotes);
        } else {
            console.log("failed request" + xhr.status)
        }
    }
    xhr.send();
}

let addNote = (data) => {
    xhr.open("POST", 'http://localhost:3000/api/v1/notes');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onload = () => {
        if (xhr.status == 200) {
            console.log(xhr.response);
            deleteNotes(getNotes);
        } else {
            console.log("failed request" + xhr.status)
        }
    }
    xhr.send(JSON.stringify(data));
}

form_data.addEventListener("submit", (e)=>{
    e.preventDefault();
    let formdata = new FormData(e.target);
    let dataset = {};
    for(let data of formdata.entries()){
        dataset[data[0]] = data[1];
    }
    addNote(dataset);
});

let deleteNotes = (cb) =>{
    var elem = document.querySelectorAll(".note");
    for (let i = 0; i < elem.length; i++) {
        app.removeChild(elem[i]);
    }
    cb();
}

getNotes();